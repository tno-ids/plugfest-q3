# TNO Connector - Plugfest Q3 Hands-on

This repository contains information for the hands-on session during the IDSA Plugest Q3-2021.

## Requirements

For the TNO connector to run you'll need:
- Docker
- Postman to interact with the connector
- A valid IDS certificate (a valid localhost certificate is configured by default)

## Configuration

The configuration of the TNO Connector is located in the `application.yaml` file.

The default configuration is ready to go and contains the localhost certificate.

If you'd want to use you own IDS certificate, valid in the AISEC DAPS, you can configure this by modifying the following lines in `application.yaml`:
- Line 23: Base64 encoded PEM* string of your certificate. Should start with: `LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0t`
- Line 24: Base64 encoded PEM* string of your private key. Should start with: `LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0t`
- Line 2: The IDS identifier corresponding to the configured key


\* PEM Certificate in the form of:
```
-----BEGIN CERTIFICATE-----
...
-----END CERTIFICATE-----
```
And PEM Private Key in the form of:
```
-----BEGIN PRIVATE KEY-----
...
-----END PRIVATE KEY-----
```

## Deployment

Deployment of the Connector can be done via plain Docker:

```
docker run -it --rm -v $(pwd)/application.yaml:/config/application.yaml -p 8082:8082 registry.ids.smart-connected.nl/public/core-container:plugfest
```

> _NOTE: When exposing port 8082 to a different port on your host machine, please modify the `API_URL` Collection variable from the Postman Collection accordingly._

## Usage
Download & import the [Postman Collection](https://gitlab.com/tno-ids/plugfest-q3/-/raw/main/Plugfest%20Connector%20Localhost.postman_collection.json?inline=false). The Postman Collection contains Tests to automatically extract relevant information from the responses and store them in Collection variables.

The Postman collection contains the following basic requests:
- **Retrieve remote selfdescription** (`/api/description`): To fetch the self-description of the hosted TNO Connector. Contains a test to get the first offered resource as variable.
- **Retrieve contract offer** (`/api/description`): To fetch a specific element (`requestedElement`) from the self-description, in this case of the first offered resource. Puts the first contract offer in a variable.
- **Request Contract** (`/api/artifacts/consumer/contractRequest`): Starts a negotiation process with the remote connector based on a given contract offer (that has been retrieved from the self-description). Stores the Contract Agreement ID as variable.
- **Retrieve artifact** (`/api/artifacts/consumer/artifact`): Retrieves the given artifact with an references to the agreed upon contract.

All of the requests have the following parameters:
- `connectorId`: The IDS ID of the remote connector (required)
- `accessUrl`: The access URL of the remote connector (required)
- `agentId`: The Agent ID of the organization behind the remote connector (optional).

## Note

This uses an early build of the new TNO Connector, which will be made open-source later this year. Full documentation and API descriptions will follow when it's made open-source. 
